﻿using UnityEngine;
using UnityEngine.UI;

public sealed class ContractEditor : MonoBehaviour {
	public byte playerID;
	[SerializeField]
	InputField m_editor;
	[SerializeField]
	InputField m_console;
	[SerializeField]
	Button m_executeButton;

	void Start()
	{
		//m_editor = GetComponentInChildren<InputField> ();
	}

	public void OnExecuteButtonClick()
	{
		var parser = new Parser ();
		parser.ParseExpload (m_editor.text, GameLogic.GameManager.players[playerID], m_console);
	}
}
