﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;


public struct Function
{
	public string keyWord;
	public byte args;
	public Function(string keyWord, byte args = 0)
	{
		this.keyWord = keyWord;
		this.args = args;
	}
}

public class Parser {
	public Function buyFunction = new Function("buy");
	public Function translateFunction = new Function("translate", 2);

	public void ParseExpload (string code, GameLogic.Player player, InputField debug) {
		debug.text = "";
		var separators = new char[]{ '(', ',', ')' };
		var lines = code.Split (separators, StringSplitOptions.RemoveEmptyEntries);
		for (ushort i = 0; i < lines.Length; i++) {
			Debug.Log (lines[i]);

			if (lines [i] == buyFunction.keyWord) {
				if (player.Buy ())
					debug.text += "Покупка совершена!\n";
				else
					debug.text += "Не удалось(уже куплен или не хватает денег)";
			} else if (lines [i] == translateFunction.keyWord) {
				string accepter = lines [i + 1];
				ushort money;
				if (!ushort.TryParse (lines[i+2], out money)) {
					debug.text += "Ошибка в строке " + (i + 1) + ": неверный аргумент";
					return;
				}
				if (player.Send (accepter, money))
					debug.text += "Транзакция проведена успешно!";
				else
					debug.text += "Неудача(нету такого игрока или у вас нет денег)";
				i += 3;
			}
		}
	}
}
