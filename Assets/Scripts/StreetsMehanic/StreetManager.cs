﻿using UnityEngine;
using System.Collections;

namespace GameLogic
{
public static class StreetManager {
    public static StreetGroup brownGroup = new StreetGroup();
    public static StreetGroup cyanGroup = new StreetGroup();
    public static StreetGroup purpleGroup = new StreetGroup();
    public static StreetGroup orangeGroup = new StreetGroup();
    public static StreetGroup redGroup = new StreetGroup();
    public static StreetGroup yellowGroup = new StreetGroup();
    public static StreetGroup greenGroup = new StreetGroup();
    public static StreetGroup blueGroup = new StreetGroup();
}
}