﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
namespace GameLogic
{
public class StreetGroup {

    public string groupName;
    public string color;

    public List<Street> streets = new List<Street>();
}
}