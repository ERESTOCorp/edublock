﻿using UnityEngine;

namespace GameLogic{
public struct Street {
	public readonly string name;
	public readonly StreetGroup streetGroup;
    public readonly StreetType type;
		public readonly ushort price;
	public Player owner;
		public Street(string name, StreetType type, ushort price = 0, StreetGroup streetGroup = null)
	{
		this.name = name;
		this.streetGroup = streetGroup;
		this.type = type;
			this.price = price;
		this.owner = null;	//owner = bank
	}
}
}