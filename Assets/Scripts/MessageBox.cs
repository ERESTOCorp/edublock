﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace GameLogic{
public class MessageBox : MonoBehaviour {
	[SerializeField]
	private Text title;
	[SerializeField]
	private Text message;
	[SerializeField]
	private GameObject Yesbutton;
	[SerializeField]
	private GameObject Nobutton;
		private Delegate callback;
	public enum ButtonParams
	{
		Question, OK
	}
		public void Show(string title, string message, ButtonParams @params, Delegate callback = null)
	{
		this.title.text = title;
		this.message.text = message;
			this.callback = callback;
			Debug.Log (@params);
		switch (@params) {
		case ButtonParams.Question:
				Yesbutton.SetActive (true);
				Nobutton.SetActive(true);
				break;
		case ButtonParams.OK:
			Yesbutton.SetActive (true);
			Nobutton.SetActive (false);
				break;
		} 
	}
		public void OnYesButtonClick()
		{
				GameManager.OnMessageBoxClosed (true);
			Destroy(gameObject);
		}
		public void OnNoButtonClick()
		{
				GameManager.OnMessageBoxClosed (false);
			Destroy(gameObject);
		}
}
}