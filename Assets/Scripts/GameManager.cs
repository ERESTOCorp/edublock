using UnityEngine;

namespace GameLogic
{
	public static class GameManager
	{
		public static Player[] players;
		public static void Init()
		{
			players = Object.FindObjectsOfType (typeof(Player)) as Player[];

			foreach (var player in players)
				player.Init ();
		}
		public static void CheckField(Player player, byte fieldID)
		{
			if (player.dept > 0) {
				View.MessageBox ("У вас проблемы", 
					"Вы отправляетесь в тюрьму из-за неуплаты долга!\nВаш штраф: "+player.dept.ToString(), 
					MessageBox.ButtonParams.OK);
				SendToJail (player);
				player.dept -= 100;
				return;
			}
			switch (GenerateStreets.streetData [fieldID].type) {
			case StreetType.GENERIC:
				if (GenerateStreets.streetData [fieldID].owner == null) {
					View.MessageBox ("Покупка города", 
						"Город " + GenerateStreets.streetData [fieldID].name +
						" не занят.(Цена:" +
						GenerateStreets.streetData [fieldID].price + ", ID:" + fieldID + ")", 
						MessageBox.ButtonParams.OK);		
				}
				break;
			case StreetType.INCOME_TAX:
				View.MessageBox ("Штраф", 
					"Вам необходимо заплатить штраф (Цена:" + GenerateStreets.streetData [fieldID].price + ", ID:" + fieldID + "), иначе попадете в тюрьму!", 
					MessageBox.ButtonParams.OK);
				player.dept = GenerateStreets.streetData [fieldID].price;
				break;
			case StreetType.SUPER_TAX:
				View.MessageBox ("Большой Штраф", 
					"Вам необходимо заплатить штраф (Цена:" + GenerateStreets.streetData [fieldID].price + ", ID:" + fieldID + "), иначе попадете в тюрьму!", 
					MessageBox.ButtonParams.OK);
				player.dept = GenerateStreets.streetData [fieldID].price;
				break;
				case StreetType.GO_TO_JAIL:
				View.MessageBox ("У вас проблемы", 
					"Вы отправляетесь в тюрьму!", 
					MessageBox.ButtonParams.OK);
				SendToJail (player);
				break;
			}

		}

		public static void SendToJail(Player player)
		{
			player.TranslateToField (10);
		}
		public static void OnMessageBoxClosed(bool yesButtonClicked)
		{
			if (yesButtonClicked) {
				Debug.Log ("Покупка города");
				//Покупка игроком города и написание смарт-контракта
			}
		}
	}
}