﻿using UnityEngine;
using UnityEngine.UI;
namespace GameLogic
{
public static class View {
		private static Text mediumText;
		private static GameObject messageBox;
		private static Canvas canvas;
		public static void Init()
		{
			try{
				mediumText = GameObject.Find ("CubesValue").GetComponent<Text>();
				canvas = Object.FindObjectOfType(typeof(Canvas)) as Canvas;
				messageBox = Resources.Load("HUD/MessageBox") as GameObject;
			}
			catch{
				Debug.LogError ("Text with name 'CubesValue' must be on scene;\nCanvas must be on scene'; MessgaeBox must be in 'HUD/MessageBox'");
			}
		}
		public static void ChangeCubesValue(byte value)
		{
			mediumText.text = "Значение костей: "+ value.ToString ();
		}
		public static void MessageBox(string title, string message, MessageBox.ButtonParams @params, System.Delegate callback = null)
		{
			var _messageBox = GameObject.Instantiate (messageBox, canvas.transform).GetComponent<MessageBox>();
			_messageBox.Show (title, message, @params, callback);
		}
	}
}