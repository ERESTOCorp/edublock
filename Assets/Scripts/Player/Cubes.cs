﻿using UnityEngine;
using UnityEngine.UI;

namespace GameLogic
{
	public class Cubes : MonoBehaviour {
		public ContractEditor contractEditor;
		Player[] players;
		byte value = 0;
		byte playerNow = 0;

		void Start()
		{
			contractEditor = FindObjectOfType (typeof(ContractEditor)) as ContractEditor;
			players = FindObjectsOfType (typeof(Player)) as Player[];
		}

		public void ThrowCubes()
		{
		if (playerNow >= players.Length)
			playerNow = 0;
		
		value = (byte)Random.Range (2, 12);
		View.ChangeCubesValue (value);
		players [playerNow].Move (value);
			GameManager.CheckField (players[playerNow], players[playerNow].cellNumber);
			contractEditor.playerID = playerNow;
			playerNow++;
		}
	}
}