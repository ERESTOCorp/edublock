﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace GameLogic{
public class Player : MonoBehaviour 
{
	private GenerateStreets _streetHolder;
		public byte cellNumber { get; private set;}	//Номер ячейки, на которой стоит игрок(изначально Go)
		private ushort money = 200;
		private ushort _dept;
		public ushort dept
		{
			get{ return _dept; }
			set {
				if (value < 0)
					_dept = 0;
				else
					_dept = value;
			}
		}
	byte _fieldLength = 0;
	RectTransform trans;
	public void Init()
	{
		_streetHolder = FindObjectOfType (typeof(GenerateStreets)) as GenerateStreets;
			_fieldLength = (byte)GenerateStreets.streetData.GetLength (0);	
		trans = GetComponent<RectTransform>();
		Move (0);
	}
		public bool Buy()
		{
			if (money >= GenerateStreets.streetData [cellNumber].price) {
				money -= GenerateStreets.streetData [cellNumber].price;
				GenerateStreets.streetData [cellNumber].owner = this;
				return true;
			}
			return false;
		}
		public bool Send(string accepter, ushort summ)
		{
			GameLogic.Player accepter_ = GameManager.players.First (x => x.name == accepter);

			if (accepter_ == null) {
				if (accepter == "Bank") {
					if (money >= summ) {
						money -= summ;
						return true;
					}
				}
			} else {
				if (money >= summ) {
					money -= summ;
					accepter_.money += summ;
					return true;
			}
		}
			return false;

		}
	private IEnumerator TestPlayer()
	{
		for (byte i = 0; i < _fieldLength * 2; i++) {
			Move (1);
			yield return new WaitForSeconds (0.25f);
		}
	}
		public void Move(byte cellsCount/*Кол-во ячеек, которые пройдет игрок за ход*/)
		{
			if ((byte)(cellNumber + cellsCount) >= _fieldLength) {
				//Игрок прошел круг
				cellNumber = (byte)(cellNumber + cellsCount - _fieldLength);
			} else
				cellNumber += cellsCount;
			StartCoroutine (SmoothTranslate ());

		}
		public void TranslateToField(byte fieldID)
		{
			if ((byte)(fieldID) >= _fieldLength) {
				Debug.Log ("Такого поля нет");
			} else
				cellNumber = fieldID;
			StartCoroutine (SmoothTranslate ());

		}
	public IEnumerator SmoothTranslate()
	{
		var epsilon = 0.001f;
			//Debug.Log (trans.anchoredPosition.ToString ());
			//Debug.Log (_streetHolder[cellNumber].ToString ());

		while (Vector2.Distance(trans.anchoredPosition, _streetHolder[cellNumber]) > epsilon) {
			trans.anchoredPosition = Vector2.Lerp (trans.localPosition, _streetHolder [cellNumber], 0.35f);
			yield return new WaitForEndOfFrame ();
		}
	}
}
}