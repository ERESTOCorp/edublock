﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PointArrayCreator : MonoBehaviour {

	public RectTransform start;
	private RectTransform previous;
	public byte count = 0;
	public Vector2 Otstyp;

	void OnGUI()
	{
		if (GUI.Button (new Rect(new Vector2(0,0), new Vector2(50,150)), "Do Array")) {
			previous = start;
			for (byte i = 0; i < count; i++) {
				RectTransform go = Instantiate (previous.gameObject, previous.position, previous.rotation, transform).GetComponent<RectTransform>();
				go.anchoredPosition = previous.anchoredPosition + Otstyp;
				go.name = i.ToString();
				previous = go;
			}
		}
	}
}
